$(document).ready(() => {
  function validateEmail($email) {
    const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
  }

  $(".btn-login").click(() => {
    const emailAddress = $(".emailLogin").val();
    if (!validateEmail(emailAddress) || emailAddress === "") {
      alert("Por favor insira um e-mail válido.");
    } else {
      const url = `${window.location.hostname}/dashboard.html`;
      window.location.replace(url);
    }
  });

  $(".registerBtn").click(() => {
    const emailAddress = $(".emailRegister").val();
    if (!validateEmail(emailAddress) || emailAddress === "") {
      alert("Por favor insira um e-mail válido.");
    } else {
      const url = `${window.location.hostname}/login.html`;
      window.location.replace(url);
    }
  });

  $(".addFormPress, .addFormAli, .addFormEx, .addFormPeso ").submit(() => {
    let isFormValid = true;

    $(".required input").each(function() {
      if ($.trim($(this).val()).length === 0) {
        $(this).addClass("highlight");
        isFormValid = false;
      } else {
        $(this).removeClass("highlight");
      }
    });

    if (!isFormValid) {
      $(".addRegErrorMsg").text("Um dos campos não foi preenchido, da uma conferida de novo aí :)");
      $(".addRegErrorMsg").show();
    }
    return isFormValid;
  });

  $(".number").keypress(function(event) {
    const $this = $(this);
    if (
      (event.which != 44 || $this.val().indexOf(",") != -1) &&
      ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))
    ) {
      event.preventDefault();
    }

    const text = $(this).val();
    if (event.which === 44 && text.indexOf(",") === -1) {
      setTimeout(() => {
        if ($this.val().substring($this.val().indexOf(",")).length > 3) {
          $this.val($this.val().substring(0, $this.val().indexOf(",") + 3));
        }
      }, 1);
    }

    if (
      text.indexOf(",") != -1 &&
      text.substring(text.indexOf(",")).length > 2 &&
      (event.which != 0 && event.which != 8) &&
      $(this)[0].selectionStart >= text.length - 2
    ) {
      event.preventDefault();
    }
  });

  $(".tipoRegistro").change(() => {
    let str = "";
    $("select option:selected").each(function() {
      str += `${$(this).text()} `;
    });
    if (str === "Adicionar Registro de Peso ") {
      $(".addFormPress").hide();
      $(".addFormAli").hide();
      $(".addFormEx").hide();
      $(".addFormPeso").show();
    } else if (str === "Adionar Registro de Pressão ") {
      $(".addFormPeso").hide();
      $(".addFormAli").hide();
      $(".addFormEx").hide();
      $(".addFormPress").show();
    } else if (str === "Adionar Registro de Alimentação ") {
      $(".addFormPeso").hide();
      $(".addFormPress").hide();
      $(".addFormEx").hide();
      $(".addFormAli").show();
    } else if (str === "Adionar Registro de Atividade ") {
      $(".addFormPeso").hide();
      $(".addFormPress").hide();
      $(".addFormAli").hide();
      $(".addFormEx").show();
    }
  });

  $(() => {
    $("#seconds").spinner({
      spin(event, ui) {
        if (ui.value >= 60) {
          $(this).spinner("value", ui.value - 60);
          $("#minutes").spinner("stepUp");
          return false;
        } else if (ui.value < 0) {
          $(this).spinner("value", ui.value + 60);
          $("#minutes").spinner("stepDown");
          return false;
        }
      },
    });
    $("#minutes").spinner({
      spin(event, ui) {
        if (ui.value >= 60) {
          $(this).spinner("value", ui.value - 60);
          $("#hours").spinner("stepUp");
          return false;
        } else if (ui.value < 0) {
          $(this).spinner("value", ui.value + 60);
          $("#hours").spinner("stepDown");
          return false;
        }
      },
    });
    $("#hours").spinner({
      min: 0,
    });
  });

  $(".showAddRegModalBtn").click(() => {
    $(".modal-overlay").show();
    $(".modal").show();
  });

  $(".closeRegModalBtn").click(() => {
    $(".addFormPeso").hide();
    $(".addFormAli").hide();
    $(".addFormEx").hide();
    $(".addFormPress").hide();
    $(".modal-overlay").hide();
    $(".addRegErrorMsg").hide();
    $(".modal").hide();
  });
});
